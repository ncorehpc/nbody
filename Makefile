###############################################################################
# Makefile for the OpenMP version of GalaxSee
#
# By default, include X display.
#
# Add NO_X11=1 to omit X libraries.
###############################################################################

#
# Variables and Flags
#

include make.rules

NO_X11=1

ifndef NO_X11
LIBS     += -lX11
LDFLAGS  += -L/usr/X11R6/lib
else
CFLAGS   += -DNO_X11
endif

LIBS     += -lm
LDFLAGS  += $(CLINKFLAGS) $(LIBS)

PROGRAM   = galaxsee
SRCS      = galaxsee.c nbody.c text11.c mem.c
OBJS      = $(SRCS:.c=.o)		# object file

#
# Targets
#

default: all

all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) -o $(PROGRAM) $(SRCS) $(CFLAGS) $(LDFLAGS)

clean:
	/bin/rm -f $(OBJS) $(PROGRAM)

