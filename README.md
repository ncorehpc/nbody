# GalaxSEE OpenMP

Galaxsee is an OpenMP enabled n-body simulation from the NCSA's BlueWaters program:

http://www.shodor.org/petascale/materials/modules/

# HPSC Build Instructions

Building the Example
------------------

make ARCH=<arch> suite

where `arch` is one of:

riscv64 (X280)
x86_x64 (the native machine type)
aarch64 (zcu102 A-53)


